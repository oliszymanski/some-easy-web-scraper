import com.gargoylesoftware.htmlunit.*;			// for client and https requesting
import com.gargoylesoftware.htmlunit.html.*;

import java.io.IOException;		// built-in libraries
import java.util.ArrayList;
import java.util.List;




class User extends Scraper{			// user (test) class
	public User(String url){
		super(url);		// super():	reference variable which is used to get a parent class constructor
	}
}


public class Scraper {
	public static String url;		// defined url;

	public Scraper(String url) {
		Scraper.url = url;
	}



	public List<String> getAnchors(){
		/*
		* description:
		* 		getting anchors from the web and adding them to a list
		* 		of HtmlAnchor type;
		*
		*
		* returns:
		*		dataList;
		* */

		List<String> dataList = new ArrayList<>();		// list for all links on the webpage


		try {
			WebClient webClient = new WebClient(BrowserVersion.FIREFOX);
			HtmlPage page = webClient.getPage(Scraper.url);

			List<HtmlAnchor> linksList = page.getAnchors();		// list of anchors


			for (HtmlAnchor link : linksList){
				String href = link.getHrefAttribute();

				System.out.println("href value: " + href);
				dataList.add( href );
			}
		}

		catch(Exception e){
			System.out.println("Couldn't find any links href attribute values");
		}

		return dataList;
	}




	public static void getPageTitle() throws IOException {
		/*
		* description:
		* 		getting the webpage name;
		*
		* returns:
		* 		--
		* */

		WebClient webClient = new WebClient(BrowserVersion.FIREFOX);
		HtmlPage page = webClient.getPage(Scraper.url);
		String title = page.getTitleText();



		System.out.println( "Web page title: " + title );
	}




	// for the test method
	public static void main(String[] args) throws IOException {
		Scraper myScraper = new Scraper("https://www.sexiarz.pl/oferta/");

		List<String> ls = myScraper.getAnchors();
		getPageTitle();

		System.out.println(ls);
	}
}